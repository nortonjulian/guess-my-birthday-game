from random import randint


name = input("Hi! What's your name?")

for guess_number in range(1, 6):
    birth_month = randint(1, 12)
    birth_year = randint(1924, 2004)
    print("Guess", guess_number, ": were you born in", birth_month, "/", birth_year, "?")

    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Goodbye.")
    else:
        print("Drat! Lemme try again!")
# # Guess 2
# birth_month = randint(1, 12)
# birth_year = randint(1924, 2004)
# print("Guess 1 :", name, "were you born in", birth_month, "/", birth_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")
# # Guess 3
# birth_month = randint(1, 12)
# birth_year = randint(1924, 2004)
# print("Guess 1 :", name, "were you born in", birth_month, "/", birth_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")

# # Guess 4
# birth_month = randint(1, 12)
# birth_year = randint(1924, 2004)
# print("Guess 1 :", name, "were you born in", birth_month, "/", birth_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")

# # Guess 5
# birth_month = randint(1, 12)
# birth_year = randint(1924, 2004)
# print(f"Guess 1 :", name, "were you born in", birth_month, "/", birth_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("I have other things to do. Goodbye.")
